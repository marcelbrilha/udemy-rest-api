const gulp = require('gulp')
const clean = require('gulp-clean')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('tsconfig.json')

gulp.task('default', ['compile'])

gulp.task('compile', ['clean', 'build'], () =>
  tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest('dist')))

gulp.task('clean', () =>
  gulp
    .src('dist')
    .pipe(clean()))

gulp.task('copy-migration', () =>
  gulp.src('server/config/config.json')
    .pipe(gulp.dest('dist/config')))

gulp.task('build', ['copy-migration'], () =>
  gulp.src('server/migrations/*')
    .pipe(gulp.dest('dist/migrations')))
