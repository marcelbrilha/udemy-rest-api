const server = require('../dist/api/api').default
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
const db = require('../dist/models')
const JWT_SECRET = require('../dist/config/env/config')().secret
const jwt = require('jsonwebtoken')

chai.use(chaiHttp)

describe('Users', () => {
  let token
  let userId

  beforeEach(() => {
    return db.sequelize.sync({ force: true })
      .then(rows => db.Author.destroy({where: {}}))
      .then(rows => db.Post.destroy({where: {}}))
      .then(rows => db.User.destroy({where: {}}))
      .then(rows => db.User.create(
        {
          name: 'mocha',
          email: 'mocha@email.com',
          password: '123'
        }
      ))
      .then(user => {
        userId = user.dataValues.id

        const payload = {
          sub: userId
        }
        
        token = jwt.sign(payload, JWT_SECRET)
      })
  })

  it('should return all users', () => {
    chai.request(server)
      .get('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        const users = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(users).to.be.an('array')
        expect(users[0]).to.have.keys(['id', 'name', 'email', 'password'])
        expect(users[0].name).to.be.equal('mocha')
        expect(users[0].email).to.be.equal('mocha@email.com')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return unauthorized', () => {
    chai.request(server)
      .get('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', 'unauthorized')
      .then(response => {
        expect(response.statusCode).to.have.status(401)
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return user per id', () => {
    chai.request(server)
      .get(`/api/users/${userId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        const user = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(user).to.be.an('object')
        expect(user).to.have.keys(['id', 'name', 'email', 'password', 'createdAt', 'updatedAt'])
        expect(user.name).to.be.equal('mocha')
        expect(user.email).to.be.equal('mocha@email.com')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should create new user', () => {
    chai.request(server)
      .post('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'mocha 2',
        email: 'mocha2@email.com',
        password: '123'
      })
      .then(response => {
        const user = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(user).to.be.an('object')
        expect(user).to.have.keys(['id', 'name', 'email', 'password', 'createdAt', 'updatedAt'])
        expect(user.name).to.be.equal('mocha 2')
        expect(user.email).to.be.equal('mocha2@email.com')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should remove user', () => {
    chai.request(server)
      .delete(`/api/users/${userId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        expect(response.statusCode).to.have.status(200)
        expect(response.body.payload).to.be.equal(1)
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should edit user', () => {
    chai.request(server)
      .put(`/api/users/${userId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'mochaedit',
        email: 'mochaedit@email.com'
      })
      .then(response => {
        const user = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(user[0]).to.be.equal(1)
        expect(user[1]).to.be.an('object')
        expect(user[1]).to.have.keys(['id', 'name', 'email', 'password', 'createdAt', 'updatedAt'])
        expect(user[1].name).to.be.equal('mochaedit')
        expect(user[1].email).to.be.equal('mochaedit@email.com')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return message name is required', () => {
    chai.request(server)
      .post('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        email: 'mocha2@email.com',
        password: '123'
      })
      .then(response => {
        const body = response.body[0]

        expect(response.statusCode).to.have.status(400)
        expect(body.msg).to.equal('The name of user is required.')
        expect(body.param).to.equal('name')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return message email is required', () => {
    chai.request(server)
      .post('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'mocha2',
        password: '123'
      })
      .then(response => {
        const body = response.body[0]

        expect(response.statusCode).to.have.status(400)
        expect(body.msg).to.equal('The email of user is required.')
        expect(body.param).to.equal('email')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return message password is required', () => {
    chai.request(server)
      .post('/api/users')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'mocha2',
        email: 'mocha2@email.com'
      })
      .then(response => {
        const body = response.body[0]

        expect(response.statusCode).to.have.status(400)
        expect(body.msg).to.equal('The password of user is required.')
        expect(body.param).to.equal('password')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return no user', () => {
    chai.request(server)
      .get(`/api/users/99999999999999`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        expect(response.statusCode).to.have.status(400)
        expect(response.body.payload).to.be.an('array').to.be.length(0)
      }, error => {
        console.log(`Error: ${error}`)
      })
  })
})
