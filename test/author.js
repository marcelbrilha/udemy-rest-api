const server = require('../dist/api/api').default
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
const db = require('../dist/models')
const JWT_SECRET = require('../dist/config/env/config')().secret
const jwt = require('jsonwebtoken')

chai.use(chaiHttp)

describe('Author', () => {
  let token
  let userId
  let authorId

  beforeEach(() => {
    return db.sequelize.sync({ force: true })
      .then(rows => db.Author.destroy({where: {}}))
      .then(rows => db.Post.destroy({where: {}}))
      .then(rows => db.User.destroy({where: {}}))
      .then(rows => db.User.create(
        {
          name: 'mocha',
          email: 'mocha@email.com',
          password: '123'
        }
      ))
      .then(user => {
        userId = user.dataValues.id

        const payload = {
          sub: userId
        }
        
        token = jwt.sign(payload, JWT_SECRET)

        return db.Author.create({
          name: 'jasmine'
        })
      })
      .then(author => {
        authorId = author.dataValues.id
      })
  })

  it('should return all authors', () => {
    chai.request(server)
      .get('/api/authors')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        const author = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(author).to.be.an('array')
        expect(author[0]).to.have.keys(['id', 'name', 'createdAt', 'updatedAt'])
        expect(author[0].name).to.be.equal('jasmine')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should create new author', () => {
    chai.request(server)
      .post('/api/authors')
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'jasmine 2'
      })
      .then(response => {
        const author = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(author).to.be.an('object')
        expect(author).to.have.keys(['id', 'name', 'createdAt', 'updatedAt'])
        expect(user.name).to.be.equal('jasmine 2')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return author per id', () => {
    chai.request(server)
      .get(`/api/authors/${authorId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        const author = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(author).to.be.an('object')
        expect(author).to.have.keys(['id', 'name', 'createdAt', 'updatedAt'])
        expect(user.name).to.be.equal('jasmine')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should edit author', () => {
    chai.request(server)
      .put(`/api/authors/${authorId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .send({
        name: 'jasmine_edit'
      })
      .then(response => {
        const author = response.body.payload

        expect(response.statusCode).to.have.status(200)
        expect(author[0]).to.be.equal(1)
        expect(author[1]).to.be.an('object')
        expect(author[1]).to.have.keys(['id', 'name', 'createdAt', 'updatedAt'])
        expect(author[1].name).to.be.equal('jasmine_edit')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should remove author', () => {
    chai.request(server)
      .delete(`/api/authors/${authorId}`)
      .set('content-type', 'application/json')
      .set('authorization', `JWT ${token}`)
      .then(response => {
        expect(response.statusCode).to.have.status(200)
        expect(response.body.payload).to.be.equal(1)
      }, error => {
        console.log(`Error: ${error}`)
      })
  })
})
