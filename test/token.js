const server = require('../dist/api/api').default
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
const db = require('../dist/models')

chai.use(chaiHttp)

describe('Token', () => {
  beforeEach(() => {
    return db.sequelize.sync({ force: true })
      .then(rows => db.Author.destroy({where: {}}))
      .then(rows => db.Post.destroy({where: {}}))
      .then(rows => db.User.destroy({where: {}}))
      .then(rows => db.User.create(
        {
          name: 'mocha',
          email: 'mocha@email.com',
          password: '123'
        }
      ))
  })

  it('should return valid token', () => {
    chai.request(server)
      .post('/auth')
      .set('content-type', 'application/json')
      .send({
        email: 'mocha@email.com',
        password: '123'
      })
      .then(response => {
        const token = response.body.token

        expect(token).to.be.string
        expect(token).to.not.undefined
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return message author is required', () => {
    chai.request(server)
      .post('/auth')
      .set('content-type', 'application/json')
      .send({
        password: '123'
      })
      .then(response => {
        const body = response.body[0]

        expect(response.statusCode).to.have.status(400)
        expect(body.msg).to.equal('The e-mail of author is required.')
        expect(body.param).to.equal('email')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })

  it('should return message password of author is required', () => {
    chai.request(server)
      .post('/auth')
      .set('content-type', 'application/json')
      .send({
        email: 'mocha@email.com'
      })
      .then(response => {
        const body = response.body[0]

        expect(response.statusCode).to.have.status(400)
        expect(body.msg).to.equal('The password of author is required.')
        expect(body.param).to.equal('email')
      }, error => {
        console.log(`Error: ${error}`)
      })
  })
})
