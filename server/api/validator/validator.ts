import { Request, Response } from 'express'

class Validator {
  body (req: Request, res: Response, prop, message) {
    req.checkBody(prop, message).exists()
    const errors = req.validationErrors()

    if (errors) {
      res.status(400).send(errors)
      return
    }
  }

  params (req: Request, res: Response, prop, message) {
    req.checkParams(prop, message).exists()
    const errors = req.validationErrors()

    if (errors) {
      res.status(400).send(errors)
      return
    }
  }
}

export default new Validator()
