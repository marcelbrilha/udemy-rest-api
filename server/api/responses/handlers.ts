import { Request, Response, ErrorRequestHandler, NextFunction } from 'express'
import { UNAUTHORIZED, OK, INTERNAL_SERVER_ERROR } from 'http-status'
import { encode } from 'jwt-simple'
import { compareSync } from 'bcrypt'

const config = require('../../config/env/config')()

class Handlers {
  authFail (req: Request, res: Response) {
    res.sendStatus(UNAUTHORIZED)
  }

  authSuccess (res: Response, credentials: any, data: any) {
    const isMatch = compareSync(credentials.password, data.password)
 
    if (isMatch) {
      res.status(OK).json({ token: encode({ id: data.id }, config.secret) })
    } else {
      res.sendStatus(UNAUTHORIZED)
    }
  }

  onError (res: Response, message: String, error: any) {
    console.log(`Error: ${error}`)
    res.status(INTERNAL_SERVER_ERROR).send(message)
  }

  onSuccess (res: Response, data: any) {
    res.status(OK).json({ payload: data })
  }

  errorHandlerApi (err: ErrorRequestHandler, req: Request, res: Response, next: NextFunction) {
    console.error(`API error handler executed: ${err}`)
  
    res.status(500).json({
      errorCode: 'ERR-001',
      message: 'Intern error in server'
    })
  }

  dbErrorHandler (res: Response, error: any) {
    console.log(`Error in db: ${error}`)
  
    res.status(INTERNAL_SERVER_ERROR).json({
      code: 'ERR-01',
      message: 'Error create user'
    })
  }
}

export default new Handlers()
