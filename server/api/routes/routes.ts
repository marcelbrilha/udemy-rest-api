import { Application } from 'express'
import AuthorRoutes from '../../modules/author/routes'
import UserRoutes from '../../modules/user/routes'
import TokenRoutes from '../../modules/auth/auth'

class Routes {
  initRoutes (app: Application, auth: any): void {
    app.route('/api/users')
      .all(auth.config().authenticate())
      .get(UserRoutes.index)
      .post(UserRoutes.create)

    app.route('/api/authors')
      .all(auth.config().authenticate())
      .get(AuthorRoutes.index)
      .post(AuthorRoutes.create)

    app.route('/api/users/:id')
      .all(auth.config().authenticate())
      .get(UserRoutes.findOne)
      .put(UserRoutes.update)
      .delete(UserRoutes.destroy)

    app.route('/api/authors/:id')
      .all(auth.config().authenticate())
      .get(AuthorRoutes.findOne)
      .put(AuthorRoutes.update)
      .delete(AuthorRoutes.destroy)

    app.route('/auth')
      .post(TokenRoutes.auth)
  }
}

export default new Routes()
