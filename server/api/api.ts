import * as express from 'express'
import * as validator from 'express-validator'
import * as morgan from 'morgan'
import * as bodyParser from 'body-parser'
import * as compression from 'compression'
import * as helmet from 'helmet'
import * as cors from 'cors'

import Routes from './routes/routes'
import Handlers from './responses/handlers'
import Auth from '../auth'
import { Application } from 'express'

class Api {
  public express: Application

  constructor () {
    this.express = express()
    this.middleware()
  }

  middleware (): void {
    this.express.use(cors({
      origin: '*',
      methods: ['GET', 'POST', 'PUT', 'DELETE'],
      allowedHeaders: ['Content-Type', 'Authorization', 'Accept-Enconding'],
      preflightContinue: false,
      optionsSuccessStatus: 204
    }))
    this.express.use(compression())
    this.express.use(helmet())
    this.express.use(morgan('dev'))
    this.express.use(bodyParser.urlencoded({ extended: true }))
    this.express.use(validator())
    this.express.use(bodyParser.json())
    this.express.use(Handlers.errorHandlerApi)
    this.express.use(Auth.config().initialize())
    this.router(this.express, Auth)
  }

  private router (app: Application, auth: any): void {
    Routes.initRoutes(app, auth)
  }
}

export default new Api().express
