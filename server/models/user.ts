import { genSaltSync, hashSync } from 'bcrypt'

export default (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: true
      }
    }
  })

  User.beforeCreate(user => hashPassword(user))
  User.beforeUpdate (user => hashPassword(user))

  const hashPassword = (user) => {
    const salt = genSaltSync(10)
    user.set('password', hashSync(user.password, salt))
  }

  return User
}
