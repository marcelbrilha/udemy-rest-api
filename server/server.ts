import { createServer } from 'http'
import Api from './api/api'

const models = require('./models')
const config = require('./config/env/config')()

const server = createServer(Api)

models.sequelize.sync().then(() => {
  server.listen(config.serverPort)
  server.on('listening', () => console.log(`Start server in port ${config.serverPort}`))
  server.on('error', (error: NodeJS.ErrnoException) => console.log(`An error has occurred: ${error}`))
})
