import { Request, Response } from 'express'
import { partial } from 'lodash'
import User from '../user/service'
import Handlers from '../../api/responses/handlers'
import validator from '../../api/validator/validator'

class TokenRoutes {
  auth (req: Request, res: Response) {
    validator.body(req, res, 'email', 'The e-mail of author is required.')
    validator.body(req, res, 'password', 'The password of author is required.')

    const credentials = {
      email: req.body.email,
      password: req.body.password
    }

    if (credentials.hasOwnProperty('email') && credentials.hasOwnProperty('password')) {
      User.getByEmail(credentials.email)
        .then(partial(Handlers.authSuccess, res, credentials))
        .catch(partial(Handlers.authFail, req, res))
    }
  }
}

export default new TokenRoutes()
