import { Request, Response } from 'express'
import { partial } from 'lodash'
import handlers from '../../api/responses/handlers'
import Author from './service'
import validator from '../../api/validator/validator'

class AuthorController {
  getAll (req: Request, res: Response) {
    Author
      .getAll()
      .then(partial(handlers.onSuccess, res))
      .catch(partial(handlers.onError, res, 'Error in get all authors'))
  }

  create (req: Request, res: Response) {
    validator.body(req, res, 'name', 'The name of author is required.')

    const author = req.body

    Author
      .create(author)
      .then(partial(handlers.onSuccess, res))
      .catch(partial(handlers.dbErrorHandler, res,))
      .catch(partial(handlers.onError, res, 'Error in insert author'))
  }

  getById (req: Request, res: Response) {
    validator.params(req, res, 'id', 'The id of author is required.')

    const authorId = parseInt(req.params.id)

    Author
      .getById(authorId)
      .then(partial(handlers.onSuccess, res))
      .catch(partial(handlers.onError, res, 'Error in get author'))
  }

  update (req: Request, res: Response) {
    validator.params(req, res, 'id', 'The id of author is required.')
    validator.body(req, res, 'name', 'The name of author is required.')

    const authorId = parseInt(req.params.id)
    const props = req.body

    Author
      .update(authorId, props)
      .then(partial(handlers.onSuccess, res))
      .catch(partial(handlers.onError, res, 'Error in update author'))
  }

  delete (req: Request, res: Response) {
    validator.params(req, res, 'id', 'The id of author is required.')

    const authorId = parseInt(req.params.id)

    Author
      .delete(authorId)
      .then(partial(handlers.onSuccess, res))
      .catch(partial(handlers.onError, res, 'Error in remove author'))
  }
}

export default new AuthorController()
