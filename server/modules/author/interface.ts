export interface IAuthor {
  id: number
  name: String
}
