import { Request, Response } from 'express'
import { partial } from 'lodash'
import User from './service'
import Handlers from '../../api/responses/handlers'
import validator from '../../api/validator/validator'

class UserController {
  getAll (req: Request, res: Response): void {
    User.getAll()
      .then(partial(Handlers.onSuccess, res))
      .catch(partial(Handlers.onError, res, 'Error fetching users'))
  }

  createUser (req: Request, res: Response): void {
    validator.body(req, res, 'name', 'The name of user is required.')
    validator.body(req, res, 'password', 'The password of user is required.')

    User.create(req.body)
      .then(partial(Handlers.onSuccess, res))
      .catch(partial(Handlers.dbErrorHandler, res))
      .catch(partial(Handlers.onError, res, 'Error create user'))
  }

  getById (req: Request, res: Response): void {
    validator.params(req, res, 'id', 'The id of user is required.')

    const userId = parseInt(req.params.id)

    User.getById(userId)
      .then(partial(Handlers.onSuccess, res))
      .catch(partial(Handlers.onError, res, 'Error fetching user per id'))
  }

  updateUser (req: Request, res: Response): void {
    validator.params(req, res, 'id', 'The id of user is required.')
    validator.body(req, res, 'name', 'The name of user is required.')

    const userId = parseInt(req.params.id)
    const body = req.body

    User.update(userId, body)
      .then(partial(Handlers.onSuccess, res))
      .catch(partial(Handlers.onError, res, 'Error update user'))
  }

  deleteUser (req: Request, res: Response): void {
    validator.params(req, res, 'id', 'The id of user is required.')

    const userId = parseInt(req.params.id)

    User.delete(userId)
      .then(partial(Handlers.onSuccess, res))
      .catch(partial(Handlers.onError, res, 'Error delete user'))
  }
}

export default new UserController()
