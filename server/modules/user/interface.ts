export interface IUser {
  readonly id: number,
  name: String,
  email: String,
  password: String
}

export interface IUserDetail extends IUser {
  id: number,
  name: String,
  email: String,
  password: String
}

export function createUser ({id, name, email, password}: any): IUser {
  return {
    id, name, email, password
  }
}

export function createUsers (data: any[]) {
  if (data) {
    return data.map(createUser)
  }

  return []
}

export function createUserById (data) {
  if (data) {
    return data
  }

  return []
}

export function createUserByEmail (data) {
  if (data) {
    return data
  }

  return []
}
